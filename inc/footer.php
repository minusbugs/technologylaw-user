<div class="bottom-footer">
	<div class="container">
		<p><a href="#" title="">TLT</a> - Copyright 2018. Developed by <a href="http://minusbugs.com">Minusbugs</a> </p>
		
		<ul>
					<li><a data-letter="Home" href="index.php" title="">Home</a>
						
					</li>
					<li><a data-letter="About" href="about.php" title="">About</a>
						
					</li>
					<li><a data-letter="Profile" href="profile.php" title="">Profile</a>
						
					</li>
					
					<li><a data-letter="Blog" href="#" title="">Blog</a>
						
					</li>
					<li><a data-letter="Articles" href="articles.php" title="">Articles</a>
						
					</li>
					<li><a data-letter="Articles" href="legal.php" title="">Legal Disclaimer </a>
						
					</li>
					
				</ul>
	</div>
</div>



</div>


<!-- MAIN JQUERY AND MODERANIZER.JS -->
<script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
<script type="text/javascript" src="js/jquery2.1.4.js"></script>	

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>

<!-- REVOLUTION JS EXTENITONS FILES -->
<script type="text/javascript" src="js/revolution/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution/revolution.extension.video.min.js"></script>

<!-- REVOLUTION JS INITIALIZATION -->
<script type="text/javascript" src="js/revolution/initialize.js"></script>

<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.poptrox.min.js"></script>
<script type="text/javascript" src="js/jquery.scrolly.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/isotope-initialize.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/enscroll-0.5.2.min.js"></script> <!-- Custom Scroll bar -->
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>