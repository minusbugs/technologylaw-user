<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Technology Law Thoughts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!-- Styles -->
	
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/revolution.css" media="screen" />
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link href="css/responsive.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/color/color.css" title="color" />
	 <link rel="icon" type="image/x-icon" href="images/fav.ico" />
</head>
<body>
<header >
			
			<div class="topbar">
				<div class="container">
					<div class="top-info">
						
						<span class="info"><i class="fa fa-phone"></i> <strong>Call Us At:</strong>    9895-8394-33</span>
					</div>
					<div class="social-links">
						<span>Follow Us On:</span>
						<a href="#" title=""><i class="fa fa-twitter"></i></a>
						<a href="https://www.linkedin.com/in/aravind-menon-7b22b2148/" title=""><i class="fa fa-linkedin"></i></a>
						
					</div>
				</div>
				</div><!-- Top Bar -->
				<div class="menubar">
					<div class="container">
						<div class="logo"><a href="#" title=""><img src="images/logo.png" alt="" /></a></div>
						<nav>
							<ul>
								<li><a data-letter="Home" href="index.php" title="">Home</a>
								
							</li>
							
							
						</li>
						<li><a data-letter="About US" href="about.php" title="">About Us</a>
						
					</li>
					<li><a data-letter="Blog" href="#" title="">Blog</a>
					
				</li>
				<li><a data-letter="Literature" href="articles.php" title="">Literature</a>
			</li>
	
		<li><a data-letter="Projects" href="#" title="">Projects</a>
	</li>
	<li><a data-letter="Contacts" href="#" title="">Contacts</a>
</li>

</ul>
</nav><!-- Navigation -->

</div>
</div><!-- Menu Bar -->
<div class="responsive-header">
<div class="responsive-bar">

<div class="logo">
	<a href="index.php" title=""><img src="images/logo.png" alt="" /></a>
</div>
<span><i class="fa fa-align-justify"></i></span>
</div><!-- Responsive Bar -->
<div class="responsive-menu">
	<ul><ul>
		<li><a data-letter="Home" href="#" title="">Home</a>
		
	</li>
	
	<li><a data-letter="About us" href="#" title="">About Us</a>
	
</li>

<li><a data-letter="Blog" href="#" title="">Blog</a>

</li>
<li><a data-letter="Literature" href="#" title="">Literature</a>
</li>
<li><a data-letter="Projects" href="#" title="">Projects</a>
</li>
<li><a data-letter="Contacts" href="#" title="">Contacts</a>
</li>
</ul></ul>
</div><!-- Responsive Menu -->
</div><!-- Responsive Header -->
</header>
<div class="page-top">

</div>