<?php
include("inc/header.php");
include("inc/db.php");
include("inc/myfunction.php");
$id=$_GET["article_id"];
$sql = "SELECT * FROM article_tbl where ArticleId=$id";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$article_day= date('d', strtotime(str_replace('-','/', $row["CreatedOn"])));
$article_month= date('M', strtotime(str_replace('-','/', $row["CreatedOn"])));
?>
<section>
	<div class="block gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="blog-detail">
						<div class="detail-img">
							<img src="<?php echo $row["ArticleImage"]==""?"http://placehold.it/1170x496": $row["ArticleImage"] ?>" alt="" />
							<div class="blog-author"><img src="http://placehold.it/52x52" alt="" /></div>
						</div>
						<div class="blog-detail-desc">
							<div class="blog-date"><?php echo $article_day; ?> <span><?php echo $article_month; ?></span></div>
							<div class="blog-text">
								<ul>
									<li><i class="fa fa-tag"></i><a tabindex="0" href="#" title="">Articles</a> 
									</li>
									<li><i class="fa fa-comment"></i>0 comments</li>
								</ul>
								<h2 class="blog-title"><?php echo $row["ArticleTittle"]; ?></h2>
								<?php echo $row["ArticleDescription"]; ?>
							</div>
						</div><!-- Blog Detail Description -->
						<div class="contact-form">
							<h3 class="simple-title">LEAVE A <i>COMMENT</i></h3>
							<form>
								<div class="row">
									<div class="col-md-6"><input type="text" placeholder="Full Name"></div>
									<div class="col-md-6"><input type="text" placeholder="Phone Number"></div>
									<div class="col-md-12"><input type="text" placeholder="Email"></div>
									<div class="col-md-12"><textarea placeholder="Details" rows="4"></textarea></div>
									<button id="submit" type="submit" class="button" title="">SEND MESSAGE</button>
								</div>
							</form>
						</div><!-- Form -->		
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include("inc/footer.php");
	?>