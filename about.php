<?php 
include("inc/header.php");

?>
<section>

	<div class="block gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="team-details">
						<div class="team-img"><img src="http://placehold.it/570x702" alt="" /></div>
						<div class="team-description">
							<span>Head- Legal</span>
							<h2 class="team-title">Aravind Menon</h2>
							<div class="team-info-box">
								<ul>
									<li><span><i class="fa fa-phone"></i> Phone No:</span> <i>9895-8394-33</i></li>
									<li><span><i class="fa fa-envelope"></i> Email Id:</span> <i>	aravindmenonsu3005@yahoo.co.in</i></li>
									
								</ul>
								<div class="team-social">
									<a href="#" title=""><i class="fa fa-facebook"></i></a>
									<a href="#" title=""><i class="fa fa-twitter"></i></a>
									<a href="#" title=""><i class="fa fa-skype"></i></a>
									<a href="#" title=""><i class="fa fa-flickr"></i></a>
								</div>
							</div><!-- Team Info Box -->
							<p>Technology Law Thoughts is a techno-legal solution provider. It aims to cater multiple technology and
law related services to both legal fraternity and technology industry..</p>
						</div>
					</div><!-- Team Detail -->
				</div>
			</div>
		</div>
	</div>

</section>


<?php
include("inc/footer.php");
?>