<?php
include("inc/db.php");
include("inc/myfunction.php");
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Technology Law Thoughts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!-- Styles -->
	<link rel="icon" type="image/x-icon" href="images/fav.ico" />
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/revolution.css" media="screen" />
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/slick.css" type="text/css" />
	<link href="css/responsive.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/color/color.css" title="color" />
	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/revolution/settings.css">
	<link rel="stylesheet" type="text/css" href="css/revolution/layers.css">
	<link rel="stylesheet" type="text/css" href="css/revolution/navigation.css">
</head>
<body>
	<div class="theme-layout">
		<div class="header-height"></div>
		<header >
			
			<div class="topbar">
				<div class="container">
					<div class="top-info">
						
						<span class="info"><i class="fa fa-phone"></i> <strong>Call Us At:</strong>    9895-8394-33</span>
					</div>
					<div class="social-links">
						<span>Follow Us On:</span>
						<a href="#" title=""><i class="fa fa-twitter"></i></a>
						<a href="https://www.linkedin.com/in/aravind-menon-7b22b2148/" title=""><i class="fa fa-linkedin"></i></a>
						
					</div>
				</div>
				</div><!-- Top Bar -->
				<div class="menubar">
					<div class="container">
						<div class="logo"><a href="#" title=""><img src="images/logo.png" alt="" /></a></div>
						<nav>
							<ul>
								<li><a data-letter="Home" href="index.php" title="">Home</a>
								
							</li>
							
							
						</li>
						<li><a data-letter="About US" href="about.php" title="">About Us</a>
						
					</li>
					<li><a data-letter="Blog" href="#" title="">Blog</a>
					
				</li>
				<li><a data-letter="Literature" href="articles.php" title="">Literature</a>
			</li>
			
			<li><a data-letter="Projects" href="#" title="">Projects</a>
		</li>
		<li><a data-letter="Contacts" href="#" title="">Contacts</a>
	</li>
</ul>
</nav><!-- Navigation -->
</div>
</div><!-- Menu Bar -->
<div class="responsive-header">
<div class="responsive-bar">
	<div class="logo">
		<a href="index.php" title=""><img src="images/logo.png" alt="" /></a>
	</div>
	<span><i class="fa fa-align-justify"></i></span>
	</div><!-- Responsive Bar -->
	<div class="responsive-menu">
		<ul><ul>
			<li><a data-letter="Home" href="#" title="">Home</a>
			
		</li>
		
		<li><a data-letter="About us" href="#" title="">About Us</a>
		
	</li>
	<li><a data-letter="Blog" href="#" title="">Blog</a>
</li>
<li><a data-letter="Literature" href="#" title="">Literature</a>
</li>
<li><a data-letter="Projects" href="#" title="">Projects</a>
</li>
<li><a data-letter="Contacts" href="#" title="">Contacts</a>
</li>
</ul></ul>
</div><!-- Responsive Menu -->
</div><!-- Responsive Header -->
</header>
<section>
<div class="block no-padding">
<div class="">
<div class="row">
<div class="col-md-12">
<div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
	<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
	<div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
		<ul>
			<li data-index="rs-396" data-transition="parallaxhorizontal" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/banne1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Slide 1" data-description="">
				<!-- MAIN IMAGE -->
				<img src="images/banne1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
				<!-- LAYER NR. 1 -->
				<div class="tp-caption layer1 tp-resizeme"
					id="slide1-layer1"
					data-x="700" data-hoffset=""
					data-y="106" data-voffset="-150"
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
					data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="1000"
					data-splitin="chars"
					data-splitout="none"
					data-responsive_offset="on"
					data-elementdelay="0.05"
					style="font-size:16px; letter-spacing:0.6px;">Law & Technology
				</div>
				<!-- LAYER NR. 2 -->
				<div class="tp-caption layer2 tp-resizeme"
					id="slide1-layer2"
					data-x="700" data-hoffset=""
					data-y="140" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;tO:0% 50%;"
					data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					data-start="2000"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					data-elementdelay="0.05"
					style="font-size:50px;letter-spacing:1px;line-height:70px;">GET <span>LEGAL</span> HELP <br/> <strong>IMMEDIATELY</strong>
				</div>
				<!-- LAYER NR. 3 -->
				<div class="tp-caption layer3 tp-resizeme"
					id="slide1-layer3"
					data-x="700" data-hoffset=""
					data-y="300" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="3000"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="font-size:15px; letter-spacing:0.3px; line-height:32px;">
				</div>
				<!-- LAYER NR. 4 -->
				<a href="#" class="tp-caption layer4 tp-resizeme rs-parallaxlevel-9"
					id="slide1-layer4"
					data-x="700" data-hoffset=""
					data-y="410" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="3300"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="font-size:13px; padding:20px 40px; letter-spacing:0.4px;">NEED LEGAL ASSISTANCE
				</a>
			</li>
			<li data-index="slide1" data-transition="parallaxhorizontal" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/banne2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Slide 2" data-description="">
				<img src="images/banne2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina />
				<!-- LAYER NR. 1 -->
				<div class="tp-caption layer1 tp-resizeme"
					id="slide2-layer1"
					data-x="center" data-hoffset=""
					data-y="106" data-voffset="-150"
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
					data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="1000"
					data-splitin="chars"
					data-splitout="none"
					data-responsive_offset="on"
					data-elementdelay="0.05"
					style="font-size:16px; letter-spacing:0.6px;">Law & Technology -News & Views
				</div>
				<!-- LAYER NR. 2 -->
				<div class="tp-caption layer2 tp-resizeme"
					id="slide2-layer2"
					data-x="center" data-hoffset=""
					data-y="140" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;tO:0% 50%;"
					data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					data-start="2000"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					data-elementdelay="0.05"
					style="font-weight:700; font-size:50px;letter-spacing:1px;line-height:70px;"><span>INVESTIGATE</span> POWER
				</div>
				<!-- LAYER NR. 3 -->
				<div class="tp-caption layer2 tp-resizeme"
					id="slide2-layer3"
					data-x="center" data-hoffset=""
					data-y="200" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;tO:0% 50%;"
					data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					data-start="2000"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					data-elementdelay="0.05"
					style="font-weight:700; font-size:50px;letter-spacing:1px;line-height:70px;">OF YOUR VOICE
				</div>
				<!-- LAYER NR. 4 -->
				<div class="tp-caption layer3 tp-resizeme"
					id="slide2-layer4"
					data-x="center" data-hoffset=""
					data-y="280" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="3000"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="text-align:center; font-size:15px; letter-spacing:0.3px; line-height:32px;">
				</div>
				<!-- LAYER NR. 4 -->
				<a href="#" class="tp-caption layer4 tp-resizeme rs-parallaxlevel-9"
					id="slide2-layer5"
					data-x="center" data-hoffset=""
					data-y="410" data-voffset=""
					data-width="['auto','auto','auto','auto']"
					data-height="['auto','auto','auto','auto']"
					data-transform_idle="o:1;"
					data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
					data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-start="3300"
					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="font-size:13px; padding:20px 40px; letter-spacing:0.4px;">NEED LEGAL ASSISTANCE
				</a>
			</li>
			<!-- SLIDE  -->
		</ul>
	</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section>
<div class="block remove-gap">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="services-wrapper overlap">
	<div class="service-box">
		<i class="fa fa-user"></i>
		<div class="service-detail">
			<span>Request </span>
			<h4>Legal Assistants</h4>
			<p>Vestibulum at magna tellu sviva...</p>
		</div>
		</div><!-- Service Box -->
		<!-- <div class="service-box">
					<i class="fa fa-cloud"></i>
					<div class="service-detail">
								<span>Lawyer Strugle</span>
								<h4>Case Investigation</h4>
								<p>Vestibulum at magna tellu sviva...</p>
					</div>
			</div> --><!-- Service Box -->
			<div class="service-box">
				<i class="fa fa-gavel"></i>
				<div class="service-detail">
					<span>People  Search</span>
					<h4>Search Directory</h4>
					<p>Vestibulum at magna tellu sviva...</p>
				</div>
				</div><!-- Service Box -->
				</div><!-- Service Wrapper -->
			</div>
		</div>
	</div>
</div>
</section>
<section>
<div class="block remove-gap">
	<div class="">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<span><strong>About</strong></span>
					<h2>Technology Law Thoughts</h2>
					<p>Technology Law Thoughts is a techno-legal solution provider. It aims to cater multiple technology and law related services to both legal fraternity and technology industry. </p>
				</div>
				<div class="addbar-wrapper">
					<div class="addbar">
						<p>Get Your Free Consultation</p>
						<a href="#" title="">HIRE ME IMMEDIATELY</a>
						<p>Available 24/7</p>
					</div>
					</div><!-- Add Bar -->
					<div class="story">
						<div class="story-img"><img src="images/img_2.jpg" alt="" /></div>
						<div class="container">
							<div class="row">
								<div class="col-md-7">
									<div class="story-detail">
										<span>Most Recent Articles</span>
										<?php
										$sql = "SELECT * FROM article_tbl order by CreatedOn desc limit 1";
										$result = $conn->query($sql);
										$row = $result->fetch_assoc();
										
										$str = $row["ArticleDescription"];
										?>
										<h4><?php echo $row["ArticleTittle"]; ?></h4>
										<p><?php
												
										echo limit_text($str,50); ?></p>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="block blackish">
		<div class="parallax" data-velocity="-.1" style="background: rgba(0, 0, 0, 0) url(http://placehold.it/1920x900) no-repeat 50% 0;"></div>
		<div class="container fancy">
			<div class="row">
				<div class="col-md-12">
					<div class="fun-facts">
						<div class="row">
							<div class="col-md-3">
								<div class="fact">
									<img src="images/fact1.png" alt="" />
									<span>700+</span>
									<i>HAPPY CLIENTS</i>
									</div><!-- Fact -->
								</div>
								<div class="col-md-3">
									<div class="fact">
										<img src="images/fact1.png" alt="" />
										<span>89%</span>
										<i>CASES SUCCESS</i>
										</div><!-- Fact -->
									</div>
									<div class="col-md-3">
										<div class="fact">
											<img src="images/fact1.png" alt="" />
											<span>$339K</span>
											<i>RECOVERED</i>
											</div><!-- Fact -->
										</div>
										<div class="col-md-3">
											<div class="fact">
												<img src="images/fact1.png" alt="" />
												<span>289+</span>
												<i>CASES DONE</i>
												</div><!-- Fact -->
											</div>
										</div>
										</div><!-- Fun Fact -->
									</div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="block">
							<div class="parallax png" data-velocity=".2" style="background: rgba(0, 0, 0, 0) url(images/lawyer.png) no-repeat 50% 0;"></div>
							<div class="row">
								<div class="col-md-12">
									<div class="title">
										<!-- <span><strong>27 Years</strong> Experience in</span> -->
										<h2>Services Offered </h2>
										<p>Major service we offered are listed below </p>
									</div>
									<div class="practice-areas">
										<div class="practice-boxes masonary">
											<div class="practice-box gap2">
												<i>01</i>
												<img src="images/practice1.png" alt="" />
												<div class="practice-info">
													<span>Many Years Of Exp</span>
													<h3>Cyber Crime Forensics</h3>
													<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
												</div>
												</div><!-- Practice Bok -->
												<div class="practice-box hor dark1 gap1">
													<i>02</i>
													<img src="images/practice2.png" alt="" />
													<div class="practice-info">
														<span>Many Years Of Exp</span>
														<h3>Techno-legal consultancy </h3>
														<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
													</div>
													</div><!-- Practice Bok -->
													<div class="practice-box">
														<i>03</i>
														<img src="images/practice3.png" alt="" />
														<div class="practice-info">
															<span>Many Years Of Exp</span>
															<h3>Statutory compliance </h3>
															<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
														</div>
														</div><!-- Practice Bok -->
														<div class="practice-box hor dark2">
															<i>04</i>
															<img src="images/practice4.png" alt="" />
															<div class="practice-info">
																<span>Many Years Of Exp</span>
																<h3>Privacy Solutions </h3>
																<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
															</div>
															</div><!-- Practice Box -->
															<div class="practice-box dark1">
																<i>05</i>
																<img src="images/practice2.png" alt="" />
																<div class="practice-info">
																	<span>Many Years Of Exp</span>
																	<h3>Data Security</h3>
																	<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
																</div>
																</div><!-- Practice Bok -->
																<div class="practice-box hor dark1">
																	<i>06</i>
																	<img src="images/practice2.png" alt="" />
																	<div class="practice-info">
																		<span>Many Years Of Exp</span>
																		<h3>Training </h3>
																		<!-- <p>Vestibulum at magna tellus ivamus...</p> -->
																	</div>
																	</div><!-- Practice Bok -->
																	<div class="practice-box">
																		<i>07</i>
																		<img src="images/practice2.png" alt="" />
																		<div class="practice-info">
																			<span>Many Years Of Exp</span>
																			<h3>BUSINESS LAW</h3>
																			<p>Vestibulum at magna tellus ivamus...</p>
																		</div>
																		</div><!-- Practice Bok -->
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											<section></section>
										<section></section>
									<section></section>
								<section></section>
							<section></section>
							<div class="bottom-footer">
								<div class="container">
									<p><a href="#" title="">TLT</a> - Copyright 2018. Developed by <a href="http://minusbugs.com">Minusbugs</a></p>
									<ul>
										<li><a data-letter="Home" href="index.php" title="">Home</a>
										
									</li>
									<li><a data-letter="About" href="about.php" title="">About</a>
									
								</li>
								<li><a data-letter="Profile" href="profile.php" title="">Profile</a>
								
							</li>
							
							<li><a data-letter="Blog" href="#" title="">Blog</a>
							
						</li>
						<li><a data-letter="Articles" href="articles.php" title="">Articles</a>
						
					</li>
					<li><a data-letter="Articles" href="legal.php" title="">Legal Disclaimer </a>
						
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<!-- MAIN JQUERY AND MODERANIZER.JS -->
	<script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
	<script type="text/javascript" src="js/jquery2.1.4.js"></script>
	<!-- REVOLUTION JS FILES -->
	<script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>
	<!-- REVOLUTION JS EXTENITONS FILES -->
	<script type="text/javascript" src="js/revolution/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="js/revolution/revolution.extension.video.min.js"></script>
	<!-- REVOLUTION JS INITIALIZATION -->
	<script type="text/javascript" src="js/revolution/initialize.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.poptrox.min.js"></script>
	<script type="text/javascript" src="js/jquery.scrolly.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/isotope-initialize.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/enscroll-0.5.2.min.js"></script> <!-- Custom Scroll bar -->
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		/* ============ Customer Reviews ================*/
		$('.reviews-carousel').owlCarousel({
			autoplay:true,
			autoplayTimeout:30000,
			smartSpeed:2000,
			loop:true,
			dots:false,
			nav:true,
			margin:10,
			singleItem:true,
			items:1,
			animateIn:"fadeIn",
			animateOut:"fadeOut"
		});
		/* ============ Sponsors Carousel ================*/
		$('.sponsors-carousel').owlCarousel({
			autoplay:true,
			autoplayTimeout:30000,
			smartSpeed:2000,
			loop:true,
			dots:false,
			nav:false,
			margin:10,
			items:6,
			responsive:{
				1200:{items:6},
				980:{items:4},
				768:{items:3},
				480:{items:2},
				0:{items:2}
			}
		});
		/* ============ Big Blog Carousel ================*/
		$('.big-blog-carousel').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.big-blog-thumb'
		});
		$('.big-blog-thumb').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.big-blog-carousel',
			dots: false,
			arrows:false,
			centerMode:true,
			focusOnSelect: true,
			responsive: [
				{
				breakpoint: 780,
				settings: {
				slidesToShow:2,
				slidesToScroll:1
				}
				},
						{
				breakpoint: 480,
				settings: {
				slidesToShow:1,
				slidesToScroll:1
				}
						}
			]
			});
	});
	</script>
</body>