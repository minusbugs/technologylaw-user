<?php
include("inc/header.php");
include("inc/db.php");
include("inc/myfunction.php");
?>
<section>
	<div class="block gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="blog-grid">
						<div class="row">
							<?php 
							
							$sql = "SELECT * FROM article_tbl order by CreatedOn desc";
							$result = $conn->query($sql);
							
							 while($row = $result->fetch_assoc()) {
							 	$article_date = $row["CreatedOn"];

							 	$article_date= date('d-m-Y', strtotime(str_replace('-','/', $article_date)));
							 	
							?>
							

								<div class="col-md-4">
								<div class="grid-post">
									<div class="post-img">
										<img src="<?php echo $row["ArticleImage"]==""?"http://placehold.it/370x340": $row["ArticleImage"] ?>" alt="" />
										<div class="date"><strong><?php echo $article_date; ?></strong></div>
									</div>
									<div class="post-detail">
										<div class="author-img"><img src="http://placehold.it/52x52" alt="" /></div>
										<h4><a href="article-details.php?article_id=<?php echo $row["ArticleId"];?>" title=""><?php echo limit_text($row["ArticleTittle"],3); ?></a></h4>
										<ul>
											<li><i class="fa fa-tag"></i><a title="" href="#" tabindex="0">articles</a></li>
											<li><i class="fa fa-comment"></i>0 comments</li>
										</ul>
									</div>
								</div><!-- Grid Post -->
							</div>
								<?php 
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	include("inc/footer.php");
	?>