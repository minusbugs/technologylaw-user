<?php 
include("inc/header.php");

?>
<section>

	<div class="block gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="blog-detail">
						
						<div class="blog-detail-desc">
							
							<div class="blog-text">
								
								<h2 class="blog-title">Legal Disclaimer</h2>
								<p>The website is intended to disseminate basic information about Technology Law Thoughts (TLT). The website contains information which are otherwise available in the public domain whether online or offline. While all reasonable care has been taken to ensure that all information provided herein is accurate and up-to-date, TLT is not liable nor responsible for any loss or damage caused due to any omission, exclusion or interpretation of any information contained therein as a result of any reliance placed by the reader. It is highly advised that reader who rely on this information should confirm the accuracy and veracity of the information from independent sources. </p>
								<p>This website is not intended to advertise nor solicit any clients. It is solely intended as a platform to publicise literature and other initiatives undertaken by TLT and to share the thoughts, perspectives and values of TLT. The contents contained herein should not be construed as legal advice or legal reference. Readers are advised to use their judgment, discretion in assessing the information. </p>
								
								<p>The website uses cookies to improve usability and provide good user experience. By continuing to use website you agree to use cookies. 
</p>
							</div>
						</div>

						

						

												

					</div>
				</div>
			</div>
		</div>
	</div>

</section>


<?php
include("inc/footer.php");
?>